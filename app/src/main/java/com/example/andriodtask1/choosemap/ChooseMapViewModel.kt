package com.example.andriodtask1.choosemap

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.andriodtask1.datamodel.Score

class ChooseMapViewModel : ViewModel(){
    private val _score = MutableLiveData<Score>()
    val score : LiveData<Score>
        get()=_score

    private val _eventChooseSum = MutableLiveData<Boolean>()
    val eventChooseSum : LiveData<Boolean>
        get() = _eventChooseSum

    private val _eventChooseSub = MutableLiveData<Boolean>()
    val eventChooseSub : LiveData<Boolean>
        get() = _eventChooseSub

    private val _eventChooseMulti = MutableLiveData<Boolean>()
    val eventChooseMulti : LiveData<Boolean>
        get() = _eventChooseMulti

    fun getScoreValue() = _score.value

    fun onChooseSum(){
        _eventChooseSum.value = true
    }
    fun onChooseSumFinish(){
        _eventChooseSum.value = false
    }

    fun onChooseSub(){
        _eventChooseSub.value = true
    }
    fun onChooseSubFinish(){
        _eventChooseSub.value = false
    }

    fun onChooseMulti(){
        _eventChooseMulti.value = true
    }
    fun onChooseMultiFinish(){
        _eventChooseMulti.value = false
    }
    init{
        _score.value = Score()
    }
}