package com.example.andriodtask1.datamodel

class Score(var correct: Int = 0, var wrong: Int = 0){
    fun addCorrect(){
        correct++
    }
    fun addWrong(){
        wrong++
    }
}